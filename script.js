let height = "20px"

const large_container = document.getElementById('largeContainer')

const startBar = document.createElement('div');
startBar.id = 'start';
startBar.classList.add('container__tower')
const offsetBar = document.createElement('div');
offsetBar.id = 'offset';
offsetBar.classList.add("container__tower")
const endBar = document.createElement('div');
endBar.id = 'end';
endBar.classList.add("container__tower")

startBar.classList.add("start_tower")
offsetBar.classList.add("towers")
endBar.classList.add("towers")

const menu = document.getElementById("menuOptions")
const reset = document.getElementById('reset');
const divSpan = document.getElementById('winnerMsg');
const divBack = document.getElementById("back")
const toBack = document.createElement("i")
const movesMsg = document.getElementById('movesCount')

const soundControl = document.getElementById("soundControls")
const buttonSoundEffect = new Audio ("./sounds-effects/button-click.wav")
const buttonToBackSoundEffect = new Audio ("./sounds-effects/to-back.wav")
const optionSoundEffect = new Audio ("./sounds-effects/options.wav")
const discFloatSoundEffect = new Audio ("./sounds-effects/float.mp3")
const discDropSoundEffect = new Audio ("./sounds-effects/drop.mp3")
const errorSoundEffect = new Audio ("./sounds-effects/error.wav")
const levelCompleteSoundEffect = new Audio ("./sounds-effects/level-completed.wav")
const backgroundSoundEffect = new Audio ("./sounds-effects/background-sound.mp3")

const userOption5 = document.getElementById("fiveDiscs")
const userOption6 = document.getElementById("sixDiscs")
const userOption7 = document.getElementById("sevenDiscs")

const disc_1 = createDiv()
const disc_2 = createDiv()
const disc_3 = createDiv()
const disc_4 = createDiv()
const disc_5 = createDiv()
const disc_6 = createDiv()
const disc_7 = createDiv()

createButtonToback()
function createDiscs() {
    disc_1.style.width = "20%"
    disc_1.style.minHeight = height
    disc_1.style.background = "blue"
    disc_1.id = 1

    disc_2.style.width = "30%"
    disc_2.style.minHeight = height
    disc_2.style.background = "red"
    disc_2.id = 2


    disc_3.style.width = "40%"
    disc_3.style.minHeight = height
    disc_3.style.background = "green"
    disc_3.id = 3


    disc_4.style.width = "50%"
    disc_4.style.minHeight = height
    disc_4.style.background = "orange"
    disc_4.id = 4


    disc_5.style.width = "60%"
    disc_5.style.minHeight = height
    disc_5.style.background = "pink"
    disc_5.id = 5

    disc_6.style.width = "70%"
    disc_6.style.minHeight = height
    disc_6.style.background = "yellow"
    disc_6.id = 6

    disc_7.style.width = "80%"
    disc_7.style.minHeight = height
    disc_7.style.background = "purple"
    disc_7.id = 7
}

let moves = 0;
let towerStart = []
let towerOffSet = []
let towerEnd = []
let currentBar = [];

function createDiv() {
    const div = document.createElement('div');
    div.classList.add('discs')
    return div
}

const addEffect = (tower) => {
    tower.classList.add('float_effect')
    tower.classList.remove("bounceInDown_effect")
}

const removeEffect = (tower) => {
    tower.classList.remove('float_effect')
    tower.classList.add("bounceInDown_effect")

}

startBar.addEventListener('click', () => {
    if (towerStart.length === 0 && currentBar.length === 0) {
    } else if (currentBar.length === 0) {
        currentBar = startBar.lastChild
        addEffect(currentBar);
        discFloatSoundEffect.play()
        discFloatSoundEffect.currentTime = 0
    } else {
        removeEffect(currentBar)
        if (towerStart.includes(currentBar)) {
            discDropSoundEffect.play()
            currentBar = []
        } else if (towerStart.length === 0) {
             countMoves()
            discDropSoundEffect.play()
            towerStart.push(currentBar)
            startBar.appendChild(currentBar)
            if (towerEnd.includes(currentBar)) {
                towerEnd.pop()
            }
            if (towerOffSet.includes(currentBar)) {
                towerOffSet.pop()
            }
            currentBar = []
        } else if (towerStart[towerStart.length - 1].id > currentBar.id) {
            countMoves()
            discDropSoundEffect.play()
            towerStart.push(currentBar)
            startBar.appendChild(currentBar)
            if (towerEnd.includes(currentBar)) {
                towerEnd.pop()
            }
            if (towerOffSet.includes(currentBar)) {
                towerOffSet.pop()
            }
            currentBar = []
        } else {
            removeEffect(currentBar)
            errorSoundEffect.play()
            currentBar = []
        }
    }

    return currentBar
})


offsetBar.addEventListener('click', () => {
    if (towerOffSet.length === 0 && currentBar.length === 0) {
    } else if (currentBar.length === 0) {
        currentBar = offsetBar.lastChild
        addEffect(currentBar);
        discFloatSoundEffect.play()
        discFloatSoundEffect.currentTime = 0
    } else {
        removeEffect(currentBar)

        if (towerOffSet.includes(currentBar)) {
            currentBar = []
            discDropSoundEffect.play()
        } else if (towerOffSet.length === 0) {
            countMoves();
            discDropSoundEffect.play()
            towerOffSet.push(currentBar)
            offsetBar.appendChild(currentBar)
            if (towerEnd.includes(currentBar)) {
                towerEnd.pop()
            }
            if (towerStart.includes(currentBar)) {
                towerStart.pop()
            }
            currentBar = []
        } else if (towerOffSet[towerOffSet.length - 1].id > currentBar.id) {
            countMoves();
            discDropSoundEffect.play()
            towerOffSet.push(currentBar)
            offsetBar.appendChild(currentBar)
            if (towerEnd.includes(currentBar)) {
                towerEnd.pop()
            }
            if (towerStart.includes(currentBar)) {
                towerStart.pop()
            }
            currentBar = []
        } else {
            removeEffect(currentBar)
            errorSoundEffect.play()
            currentBar = []
        }
    }
    return currentBar

})

endBar.addEventListener('click', () => {
    if (towerEnd.length === 0 && currentBar.length === 0) {

    } else if (currentBar.length === 0) {
        currentBar = endBar.lastChild
        addEffect(currentBar);
        discFloatSoundEffect.play()
        discFloatSoundEffect.currentTime = 0
    } else {
        removeEffect(currentBar)

        if (towerEnd.includes(currentBar)) {
            discDropSoundEffect.play()
            currentBar = []
        } else if (towerEnd.length === 0) {
            countMoves();
            discDropSoundEffect.play()
            towerEnd.push(currentBar)
            endBar.appendChild(currentBar)
            if (towerOffSet.includes(currentBar)) {
                towerOffSet.pop()
            }
            if (towerStart.includes(currentBar)) {
                towerStart.pop()
            }
            currentBar = []

        } else if (towerEnd[towerEnd.length - 1].id > currentBar.id) {
            countMoves();
            discDropSoundEffect.play()
            towerEnd.push(currentBar)
            endBar.appendChild(currentBar)
            if (towerOffSet.includes(currentBar)) {
                towerOffSet.pop()
            }
            if (towerStart.includes(currentBar)) {
                towerStart.pop()
            }
            currentBar = []
        } else {
            removeEffect(currentBar)
            errorSoundEffect.play()
            currentBar = []
        }
    }
    if (towerStart.length === 0 && towerOffSet.length === 0) {
        
         winMsg();
    }
    return currentBar
})


/* Botão reset */
const startGame = () => {
    buttonSoundEffect.play()
    backgroundSoundEffect.play()
    backgroundSoundEffect.volume = .3;
    backgroundSoundEffect.loop = true;
    playSound()
    currentBar = [];
    towerStart = [];
    towerOffSet = [];
    towerEnd = [];
    moves = 0;
    movesMsg.innerHTML = ''
    const movesCount = document.createElement('span')
    movesCount.innerText = `Movements: ${moves}`
    movesCount.classList.add('moves_count')
    movesMsg.appendChild(movesCount)

    reset.innerText = "RESET GAME"
    resetEffect()
    if (userOption5.checked === true) {
        towerStart = [disc_5, disc_4, disc_3, disc_2, disc_1]
    } else if (userOption6.checked === true) {
        towerStart = [disc_6, disc_5, disc_4, disc_3, disc_2, disc_1]
        height = "18px"
    } else if (userOption7.checked === true) {
        towerStart = [disc_7, disc_6, disc_5, disc_4, disc_3, disc_2, disc_1]
        height = "16px"
    }
    createDiscs()
    large_container.appendChild(startBar)
    large_container.appendChild(offsetBar);
    large_container.appendChild(endBar);
    divSpan.innerHTML = ''
    createDiv();
    for (let i = 0; i < towerStart.length; i++) {
        startBar.appendChild(towerStart[i])
    }
    menu.classList.add("hidden")
    divBack.classList.remove("hidden")
}

reset.addEventListener('click', startGame);

const winMsg = () => {
    backgroundSoundEffect.volume = 0
    levelCompleteSoundEffect.play()
    const spanWinner = document.createElement('span');
    spanWinner.innerText = `Well Done, you got it with ${moves} movements!`;
    spanWinner.classList.add('spanWinner');
    divSpan.style.display = "flex"
    divSpan.appendChild(spanWinner)
    return spanWinner;
}

toBack.addEventListener("click", toBackMenu)

function toBackMenu() {
    divBack.classList.add("hidden")
    document.location.reload();
    buttonToBackSoundEffect.play()
}

function createButtonToback() {
    toBack.classList.add("fas")
    toBack.classList.add("fa-arrow-circle-left")
    divBack.classList.add("hidden")
    toBack.id = "toBackMenu"
    toBack.title = "Back to Menu"
    toBack.classList.add("button_back")
    divBack.appendChild(toBack)
}


toBack.addEventListener("mouseover", () => {
    toBack.classList.add("backward_effect")
})
toBack.addEventListener("mouseout", () => {
    toBack.classList.remove("backward_effect")
})

function resetEffect() {
    disc_1.classList.remove("float_effect")
    disc_2.classList.remove("float_effect")
    disc_3.classList.remove("float_effect")
    disc_4.classList.remove("float_effect")
    disc_5.classList.remove("float_effect")
    disc_6.classList.remove("float_effect")
    disc_7.classList.remove("float_effect")
}

/*Contador de movimentos */
const countMoves = () => {
    moves++
    movesCount.innerText = `Movements: ${moves}`
    return moves
}


soundControl.addEventListener("click", () =>{

    if(soundControl.classList == "fas fa-volume-up"){
        soundControl.classList.remove("fa-volume-up")
        soundControl.classList.add("fa-volume-mute")
    } else {
        soundControl.classList.remove("fa-volume-mute")
        soundControl.classList.add("fa-volume-up")
    }
    playSound()
})

function playSound(){
    if(soundControl.classList == "fas fa-volume-up"){
        optionSoundEffect.volume = 1
        buttonSoundEffect.volume = 1
        discFloatSoundEffect.volume = .8 
        discDropSoundEffect.volume = 1
        errorSoundEffect.volume = 1
        levelCompleteSoundEffect.volume = 1 
        backgroundSoundEffect.volume = .3
        buttonToBackSoundEffect.volume = 1
    } else {
        optionSoundEffect.volume = 0
        buttonSoundEffect.volume = 0
        discFloatSoundEffect.volume = 0 
        discDropSoundEffect.volume = 0
        errorSoundEffect.volume = 0
        levelCompleteSoundEffect.volume = 0 
        backgroundSoundEffect.volume = 0
        buttonToBackSoundEffect.volume = 0

    }
}
userOption5.addEventListener("click", () => optionSoundEffect.play())
userOption6.addEventListener("click", () => optionSoundEffect.play())
userOption7.addEventListener("click", () => optionSoundEffect.play())

toBack.addEventListener("mouseover", () => {
    buttonToBackSoundEffect.play()
    buttonToBackSoundEffect.currentTime = 0
})


